package controller;

import domain.Person;

import java.util.Scanner;

public class PersonController {

    public static Person insertDataForNewPerson(Scanner keyboard){
        System.out.println("Name: ");
        String name = keyboard.nextLine();
        System.out.println("Surname: ");
        String surname = keyboard.nextLine();
        System.out.println("Age: ");
        int age = keyboard.nextInt();
        keyboard.nextLine();
        Person person = Person.builder()
                .name(name)
                .surname(surname)
                .age(age)
                .build();
        return person;
    }
}
