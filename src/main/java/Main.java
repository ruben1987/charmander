import controller.PersonController;
import domain.Person;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Insert data for new Person: ");
        Person person = PersonController.insertDataForNewPerson(keyboard);
        System.out.println("Name: "+person.getName());
        System.out.println("Surname: "+person.getSurname());
        System.out.println("Age: "+person.getAge());
    }
}
