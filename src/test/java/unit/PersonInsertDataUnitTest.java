package unit;

import domain.Person;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(JUnit4.class)
public class PersonInsertDataUnitTest {

    @Test
    public void insertDataForNewPerson(){
        Person person = Person.builder().name("Pedro").surname("Sanchez").age(25).build();
        assertEquals("Pedro",person.getName());
        assertEquals("Sanchez",person.getSurname());
        assertNotEquals(12,person.getAge());
    }
}
